<?php

namespace app\base;

class Controller
{
    public function render($view, $params = [])
    {
        return Application::$app->router->renderView($view, $params);
    }
    public function redirect($url)
    {
        header('Location: /' . $url);
        die();
    }
}
