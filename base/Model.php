<?php

namespace app\base;

abstract class Model
{

    public const REQUIRED = 'required';
    public const EMAIL = 'email';
    public const LOGIN_CREDENTIALS_MATCH = 'match';
    public const MAX_SIZE = 'max';

    public $tablename = "";

    public function load($data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $value = htmlspecialchars($value);
                $this->{$key} = $value;
            }
        }
    }

    abstract public function rules(): array;

    public $errors = [];

    public function validate()
    {
        foreach ($this->rules() as $attribute  => $rules) {
            $value = $this->{$attribute};
            foreach ($rules as $rule) {
                $ruleName = $rule;
                if (!is_string($ruleName)) {
                    $ruleName = $rule[0];
                }
                if ($ruleName === self::REQUIRED && !$value) {
                    $this->addError($attribute, self::REQUIRED);
                }
                if ($ruleName === self::EMAIL && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $this->addError($attribute, self::EMAIL);
                }
                if ($ruleName === self::MAX_SIZE && iconv_strlen($value,'UTF-8') > $rule["max"]) {
                    $this->addError($attribute, self::MAX_SIZE, $rule);
                }
            }
        }
        return empty($this->errors);
    }

    public function setTable($tablename)
    {
        $this->tablename = $tablename;
        return $tablename;
    }

    public function selectWhere($fields, $conditions = [], $start_card = null, $limit = null, $sorting = 0, $order = 0)
    {
        $query_results = [];
        $query_params = [];
        $query = "SELECT $fields from $this->tablename";

        if ($conditions) {
            $query .= " WHERE ";
            $iterator = 1;
            foreach ($conditions as $fieldname => $value) {
                $query = $query . "$fieldname = :value_$iterator and ";
                $query_params[':value_' . $iterator] = $value;
                $iterator++;
            }
            $query = substr($query, 0, strlen($query) - 5);
        }
        if ($sorting) {
            $query .= " ORDER BY $sorting";
            
        }
        if ($order) {
            $query .= " $order";
        }
        if ($limit !== null && $start_card !== null) {
            $query .= ' LIMIT :startcard, :limit';
            $query_params[':startcard'] = $start_card;
            $query_params[':limit'] = $limit;
        }
        $prepared_query = Application::$pdo->prepare($query);
        $result = $prepared_query->execute($query_params);
        if ($result) {
            $queries = $prepared_query->fetchAll();
            foreach ($queries as $index => $row) {
                $model = new $this();
                $model->load($row);
                array_push($query_results, $model);
            }
        }
        return $query_results;
    }


    public function addError($attribute, $rule, $params = [])
    {
        $message = $this->errorMessages()[$rule] ?? '';

        foreach ($params as $key => $value) {
            $message = str_replace("$key", $value, $message);
        }
        $this->errors[$attribute][] = $message;
    }

    public function hasErrors($attribute)
    {
        return $this->errors[$attribute] ?? false;
    }

    public function getFirstAttributeError($attribute)
    {
        return $this->errors[$attribute][0] ?? false;
    }

    public function errorMessages()
    {
        return [
            self::REQUIRED => 'Поле не должно быть пустым',
            self::EMAIL => 'Некорректный email',
            self::LOGIN_CREDENTIALS_MATCH => 'Неверный логин или пароль',
            self::MAX_SIZE => 'Максимальный размер поля не должен превышать max символов',

        ];
    }
}
