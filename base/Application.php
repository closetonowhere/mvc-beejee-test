<?php

namespace app\base;

class Application
{
    public static  $ROOT_DIR;
    public $router;
    public $request;
    public $responce;
    public static $app;
    public static $pdo;
    public static $page;
    public $user;
    public $userid;


    public function __construct($rootPath, $pdo, $config)
    {
        self::$app = $this;
        self::$pdo = $pdo;
        self::$ROOT_DIR = $rootPath;
        $this->request = new Request();
        $this->responce = new Responce();
        $this->router = new Router($this->request, $this->responce);
        session_start();
        $this->userid = $_SESSION["id"];
        if ($this->userid) {
            $usermodel = new $config["user"];
            $user_row = $usermodel->selectWhere("login", ['id' => $this->userid]);
            if ($user_row) {
                $this->user = $user_row[0];
            }
        }
    }

    public function run()
    {
        echo $this->router->resolve();
    }
    public static function setFlash($key, $message)
    {
        $_SESSION["FLASH"][$key] = $message;
    }

    public static function getFlash($key)
    {
        $message = $_SESSION["FLASH"][$key];
        if ($_SESSION["FLASH"][$key]) {
            unset($_SESSION["FLASH"][$key]);
            return "<div class='alert alert-success' role='alert'>$message</div>";
        }
    }

    public function setUser($user)
    {
        $this->user = $user;
        $_SESSION["id"] = $this->user->id;
    }

    public function unsetUser()
    {
        $this->user = null;
        unset($_SESSION["id"]);
    }
}
