<?php

namespace app\base;

class Responce
{
    public function setStatusCode(int $code)
    {
        http_response_code($code);
    }
}
