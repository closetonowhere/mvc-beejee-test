<?php

namespace app\models;

use app\base\Application;
use app\base\Model;

class TaskModel extends Model
{
    public $id;
    public $name;
    public $email;
    public $body;
    public $status;
    public $isredacted;

    public function rules(): array
    {
        return [
            'name' => [self::REQUIRED, [self::MAX_SIZE, 'max' => 50]],
            'email' => [self::REQUIRED, self::EMAIL, [self::MAX_SIZE, 'max' => 40]],
            'body' => [self::REQUIRED, [self::MAX_SIZE, 'max' => 300]]
        ];
    }
    public function __construct()
    {
        $this->setTable('tasks');
    }

    public function write()
    {
        if (!empty($this->id)) {
            $query = Application::$pdo->prepare("UPDATE $this->tablename SET name = :name, email = :email, body = :body, isredacted=:isredacted
                                            WHERE id = :id");
            $query->bindValue(':name', $this->name);
            $query->bindValue(':email',  $this->email);
            $query->bindValue(':body',  $this->body);
            $query->bindValue(':id',  $this->id);
            $query->bindValue(':isredacted',  $this->isredacted);
        } else {
            $query = Application::$pdo->prepare("insert into $this->tablename(name, email, body) values (:name, :email, :body)");
            $query->bindValue(':name', $this->name);
            $query->bindValue(':email',  $this->email);
            $query->bindValue(':body',  $this->body);
        }
        if ($query->execute()) {
            $query->fetchAll();
        }
    }
}
