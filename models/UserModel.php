<?php

namespace app\models;

use app\base\Application;
use app\base\Model;

class UserModel extends Model
{
    public $id;
    public $login;
    public $password;

    public function rules(): array
    {
        return [
            'login' => [self::REQUIRED],
            'password' => [self::REQUIRED]
        ];
    }
    public function __construct()
    {
        $this->setTable('user');
    }
   
    public function login()
    {
        $query = Application::$pdo->prepare("select id, password from $this->tablename where login=:login");
        $query->bindValue(':login',  $this->login);
        if ($query->execute()) {
            $user_row = $query->fetch();
            if (password_verify($this->password, $user_row["password"])) {
                $this->id = $user_row["id"];
                return true;
            }
            return false;
        }
    }
}
