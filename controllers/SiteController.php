<?php

namespace app\controllers;

use app\base\Application;
use app\base\Controller;
use app\models\TaskModel;

class SiteController extends Controller
{

    public function index($request)
    {
        if (!empty($request->getBody()['page'])) {
            Application::$page = (int) $request->getBody()['page'];
        } else {
            Application::$page = 1;
        }
        if (!empty($request->getBody()['sorting'])) {
            $sorting = $request->getBody()['sorting'];
        } else {
            $sorting = 0;
        }
        if (!empty($request->getBody()['order'])) {
            $order = $request->getBody()['order'];
        } else {
            $order = 0;
        }

        $first_item = (Application::$page - 1) * 3;
        $tasks = new TaskModel();
        $records_number = Application::$pdo->query("select count(*) from tasks")->fetch();
        if ($records_number > 0) {
            $pages_number = ceil((int)$records_number['count(*)'] / 3);
            $task_cards = $tasks->selectWhere("*", [], $first_item, 3, $sorting, $order);
        }
        return $this->render('index', [
            'cards' => $task_cards, 'pages_number' => $pages_number,
            'sorting' => $sorting, 'order' => $order
        ]);
    }
    public function task($request)
    {
        $model = new TaskModel();

        if (!empty($request->getBody()["id"])) {
            if (Application::$app->user) {

                $model = $model->selectWhere("*", ['id' => $request->getBody()['id']])[0];
                $previous_body = $model->body;
            } else {
                http_response_code('403');
                return $this->redirect('login');
            }
        }
        if ($request->isPost()) {
            $model->load($request->getBody());
            if ($model->validate()) {
                if ($request->getBody()["body"] !== $previous_body) {
                    $model->isredacted = 1;
                }
                $model->write();
                Application::setFlash('success', 'Успешное добавление или редактирвание задачи');
                return $this->redirect('');
            }
        }
        return $this->render('task', ['model' => $model]);
    }

    public function done($request)
    {
        if (Application::$app->user) {
            if ($request->isPost()) {
                $model = new TaskModel();
                $model = $model->selectWhere("*", ['id' => $request->getBody()['id']])[0];
                $query = Application::$pdo->prepare("UPDATE tasks SET status=1 WHERE id=:id");
                $query->bindValue(':id', $request->getBody()['id']);
                $result = $query->execute();
            }
        }
    }
}
