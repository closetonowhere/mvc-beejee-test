<?php

namespace app\controllers;

use app\base\Application;
use app\base\Controller;
use app\base\Model;
use app\models\UserModel;

class AuthController extends Controller
{
    public function login($request)
    {
        if (Application::$app->user) {
            $this->redirect('');
        } else {
            $model = new UserModel();
            if ($request->isPost()) {
                if (!empty($request->getBody())) {
                    $login = trim($request->getBody()["login"]);
                    $password = trim($request->getBody()["password"]);
                    $model->load(['login' => $login, 'password' => $password]);
                    if ($model->validate()) {
                        if ($model->login()) {
                            Application::$app->setUser($model);
                            $this->redirect('');
                        } else {
                            $model->addError('login', Model::LOGIN_CREDENTIALS_MATCH);
                            $model->addError('password', Model::LOGIN_CREDENTIALS_MATCH);
                        }
                    }
                }
            }
            return $this->render('login', ['model' => $model]);
        }
    }

    public function logout($request)
    {
        Application::$app->unsetUser();
        return $this->redirect('login');
    }

}
