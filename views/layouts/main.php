<?php

use app\base\Application;

?>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <title>Tasking system</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Tasks</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/">Главная <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/task">Добавить задачу</a>
        </li>

      </ul>

      <ul class="navbar-nav">
        <?php if (Application::$app->user) : ?>
          <li class="nav-item">
            <p class="nav-link"><?php echo Application::$app->user->login; ?></p>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/logout">Выйти</a>
          </li>

        <?php else : ?>
          <li class="nav-item">
            <a class="nav-link" href="/login">Логин</a>
          <?php endif; ?> </li>
      </ul>
    </div>
  </nav>
  <?php echo Application::getFlash('success'); ?>
  <div class="container">

    {{content}}

  </div>

</body>

</html>