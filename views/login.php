<form action="login" method="post" class="col-5">
  <div class="form-group">
    <label for="login">Логин</label>
    <input type="text" name="login" id="login" class="form-control
     <?php echo $model->hasErrors('login') ? ' is-invalid' : ''; ?>">
    <div class="invalid-feedback">
      <?php echo $model->getFirstAttributeError('login'); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="password">Пароль</label>
    <input type="password" name="password" id="password" class="form-control 
    <?php echo $model->hasErrors('password') ? ' is-invalid' : ''; ?>">
    <div class="invalid-feedback">
      <?php echo $model->getFirstAttributeError('password'); ?>
    </div>
  </div>

  <button type="submit" class="btn btn-primary">Войти</button>
</form>