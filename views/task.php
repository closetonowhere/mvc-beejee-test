<form action="task" method="post">
  <?php if (!empty($model->id)) : ?>
    <input type="text" name="id" value="<?php echo $model->id; ?>" hidden>
  <?php endif; ?>
  <div class="form-group">
    <label for="name">Имя</label>
    <input type="text" name="name" id="name" class="form-control
     <?php echo $model->hasErrors('name') ? ' is-invalid' : ''; ?>" value="<?php echo $model->name; ?>">
    <div class="invalid-feedback">
      <?php echo $model->getFirstAttributeError('name'); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="email">email</label>
    <input type="email" name="email" id="email" class="form-control 
    <?php echo $model->hasErrors('email') ? ' is-invalid' : ''; ?>" value="<?php echo $model->email; ?>">
    <div class="invalid-feedback">
      <?php echo $model->getFirstAttributeError('email'); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="body">Задача</label>
    <textarea type="text" name="body" id="body" class="form-control 
    <?php echo $model->hasErrors('body') ? ' is-invalid' : ''; ?>"><?php echo $model->body; ?></textarea>
    <div class="invalid-feedback">
      <?php echo $model->getFirstAttributeError('body'); ?>
    </div>
  </div>

  <button type="submit" class="btn btn-primary">Отправить</button>
</form>