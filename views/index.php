<?php

use app\base\Application;
?>
<div class="row mt-3">
  <a href="?sorting=name<?php if ($order === 'asc') {
                          echo '&order=desc';
                        } else {
                          echo '&order=asc';
                        } ?>" class="btn btn-outline-primary">Сортировать по имени
    <?php if ($order === 'asc' && $sorting === 'name') : ?>
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z" />
      </svg>
    <?php endif; ?>
    <?php if ($order === 'desc' && $sorting === 'name') : ?>
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi  bi-arrow-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z" />
      </svg>
    <?php endif; ?>
  </a>
  <div class="col-auto"> </div>

  <a href="?sorting=email<?php if ($order === 'asc') {
                            echo '&order=desc';
                          } else {
                            echo '&order=asc';
                          } ?>" class="btn btn-outline-secondary">Сортировать по email
    <?php if ($order === 'asc' && $sorting === 'email') : ?>
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z" />
      </svg>
    <?php endif; ?>
    <?php if ($order === 'desc' && $sorting === 'email') : ?>
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi  bi-arrow-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z" />
      </svg>
    <?php endif; ?>

  </a>
  <div class="col-auto"> </div>

  <a href="?sorting=status<?php if ($order === 'asc') {
                            echo '&order=desc';
                          } else {
                            echo '&order=asc';
                          } ?>" class="btn btn-outline-success">Сортировать по статусу
    <?php if ($order === 'asc' && $sorting === 'status') : ?>
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z" />
      </svg>
    <?php endif; ?>
    <?php if ($order === 'desc' && $sorting === 'status') : ?>
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi  bi-arrow-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z" />
      </svg>
    <?php endif; ?></a>

</div>
<div class="row">

  <?php foreach ($cards as $key => $task) : ?>
    <div class="card col-lg-5 mt-4">
      <div class="card-body">
        <?php if (Application::$app->user) : ?>
          <?php if (!$task->status) : ?>
            <div class="form-check float-right">
              <input class="form-check-input" type="checkbox" value="1" name="status" id="status<?php echo $task->id; ?>" data-id="<?php echo $task->id; ?>">
              <label class="form-check-label" for="status<?php echo $task->id; ?>">
              </label>
            </div>
          <?php endif; ?>
        <?php endif; ?>

        <h5 class="card-title"><?php echo $task->name; ?></h5>
        <h6 class="card-subtitle mb-2 text-muted"><?php echo $task->email; ?></h6>
        <p class="card-text"><?php echo $task->body; ?></p>
        <p class="card-text"><?php if ($task->status) {
                                  echo "<p class='text-success'>Завершена</p>";
                                } else {
                                  echo "<p class='text-danger'>Не завершена</p>";
                                }; ?></p>
        <p class="card-text"><?php if ($task->isredacted) {
                                  echo "<p class='text-info'>Редактирована администратором</p>";
                                } ?></p>
        <?php if (Application::$app->user) : ?>
          <a href="task?id=<?php echo $task->id; ?>" class="card-link">Редактировать</a>
        <?php endif; ?>
      </div>
    </div>
    <div class="col-auto"> </div>
  <?php endforeach; ?>
  <div class="card col-lg-5 mt-4">
    <div class="card-body">
      <a href="task">
        <h3 class="m-5">Добавить задачу</h3>
      </a>
    </div>
  </div>
</div>
<nav class="mt-5">
  <ul class="pagination">
    <?php for ($i = 1; $i <= $pages_number; $i++) : ?>
      <li class="page-item"><a class="page-link" href="/?page=<?php echo $i; ?><?php if ($sorting !== 0) {
                                                                                    echo '&sorting=' . $sorting . '&order=' . $order;
                                                                                  } ?>"><?php echo $i; ?></a></li>
    <?php endfor; ?>
  </ul>
</nav>
<script src='js/ajax_completion.js'></script>