<?php


require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config/db.php';

use app\base\Application;
use app\controllers\SiteController;
use app\controllers\AuthController;
use app\models\UserModel;

$config = [
    'user' => UserModel::class
];
$app = new Application(dirname(__DIR__), $pdo, $config);

$app->router->get('/', [SiteController::class, 'index']);
$app->router->get('/task', [SiteController::class, 'task']);
$app->router->post('/task', [SiteController::class, 'task']);

$app->router->get('/login', [AuthController::class, 'login']);
$app->router->post('/login', [AuthController::class, 'login']);
$app->router->get('/logout', [AuthController::class, 'logout']);


$app->router->post('/done', [SiteController::class, 'done']);


$app->run();
